public class Mutable{
    public static void main(String[] args){
        int a=26;
        System.out.println("Int a before address : "+System.identityHashCode(a));
        a=68;
        System.out.println("Int a after address : "+System.identityHashCode(a));
        System.out.println();
        String s="Hemanth";
        System.out.println("String before address : "+System.identityHashCode(s));
        s=s.replace("Hemanth", "Deelip");
        System.out.println("String after address : "+System.identityHashCode(s));
        System.out.println();
        StringBuffer c=new StringBuffer("Hello!!");
        System.out.println("StringBuffer before address : "+System.identityHashCode(c));
        c=c.replace(0,5,"Hi!!");
        System.out.println("StringBuffer address : "+System.identityHashCode(c));
    }
}
