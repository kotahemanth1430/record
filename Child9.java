import java.util.*;
abstract class Child9{
   abstract public void run();
    void eat()
    {
        System.out.println("please drink water");
    }
}
class Child10 extends Child9{
  public void run(){
    System.out.println("Its high time run!");
  }
}
class ParAbst{
    public static void main(String[] args){
        Child10 obj=new Child10();
        obj.run();
        obj.eat();
    }
}