import java.io.*;
class FileIn {
    public static void main(String[] args) {
    File inpf = new File("C:/21331A0597/text1.txt"); 
    File outf = new File("C:/21331A0597/text2.txt");
        try{
                FileInputStream in = new FileInputStream(inpf);
                FileOutputStream out = new FileOutputStream(outf);
                int data;
                while((data=in.read())!=-1){
                    out.write(data);
                }
                System.out.println("File Copying is Successful");
                in.close();
                out.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found!" );
        } catch (IOException e) {
            System.out.println("Error copying the file ");
        }catch (NullPointerException e) {
            System.out.println("One of the file paths is null");

        }
    }
}
