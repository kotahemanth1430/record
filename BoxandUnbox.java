import java.util.*;
class BoxandUnbox{
    public static void main(String[] args){
        int a=10;
        Integer b=a;//boxing
        int c=a;//unboxing
        System.out.println("The Boxing value: "+b);
        System.out.println("The UnBoxing value: "+c);
    }
}