import java.util.*;
class Exception{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
      
        try{
            System.out.println("Enter two numbers :");
            int a = input.nextInt();
            int b = input.nextInt();
            System.out.println("The value of a/b is : " +(a/b));
        }
        catch(ArithmeticException obj){
            System.out.println("Divide by zero error!");
        }
        catch(InputMismatchException obj){
            System.out.println("Invalid input! only integers are accepted");
        }
    }
}
