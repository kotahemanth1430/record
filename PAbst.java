interface Wake {
    void run();
}
class Child8 implements Wake {
    public void run() {
        System.out.println("its high time ");
    }
}
public class PAbst {
    public static void main(String[] args) {
        Wake obj1 = new Child8();
        
        obj1.run(); 
    }
}
